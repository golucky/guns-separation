/*
Copyright [2020] [https://www.stylefeng.cn]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Guns采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：

1.请不要删除和修改根目录下的LICENSE文件。
2.请不要删除和修改Guns源码头部的版权声明。
3.请保留源码和相关描述文件的项目出处，作者声明等。
4.分发源码时候，请注明软件出处 https://gitee.com/stylefeng/guns-separation
5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/stylefeng/guns-separation
6.若您的项目无法满足以上几点，可申请商业授权，获取Guns商业授权许可，请在官网购买授权，地址为 https://www.stylefeng.cn
 */
package cn.stylefeng.guns.core.pojo.jedis;

import lombok.Data;
import org.apache.commons.pool2.impl.DefaultEvictionPolicy;
import redis.clients.jedis.JedisPoolConfig;


/**
 * Jedis配置
 * 说明:类中属性包含默认值的不要在这里修改, 应该在"application.yml"中配置
 *
 * @author susu
 * @date 2020/9/24 12:37
 */
@Data
public class JedisProperties {

    private String host = "localhost";
    private int port = 6379;
    private int connectionTimeout = 2000;
    private int soTimeout = 2000;
    private String password = "";
    private int database;
    private String clientName = "jedis";
    private boolean ssl;

    private int maxTotal = 8;
    private int maxIdle = 8;
    private int minIdle = 0;
    private boolean lifo = true;
    private boolean fairness = false;
    private long maxWaitMillis = -1L;
    private long minEvictableIdleTimeMillis = 1800000L;
    private long evictorShutdownTimeoutMillis = 10000L;
    private long softMinEvictableIdleTimeMillis = -1L;
    private int numTestsPerEvictionRun = 3;
    private String evictionPolicyClassName = DefaultEvictionPolicy.class.getName();
    private boolean testOnCreate;
    private boolean testOnBorrow;
    private boolean testOnReturn;
    private boolean testWhileIdle;
    private long timeBetweenEvictionRunsMillis = -1L;
    private boolean blockWhenExhausted = true;
    private boolean jmxEnabled;
    private String jmxNamePrefix = "";
    private String jmxNameBase = "";

    public void config(JedisPoolConfig config) {
        config.setMaxTotal(maxTotal);
        config.setMaxIdle(maxIdle);
        config.setMinIdle(minIdle);
        config.setLifo(lifo);
        config.setFairness(fairness);
        config.setMaxWaitMillis(maxWaitMillis);
        config.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        config.setEvictorShutdownTimeoutMillis(evictorShutdownTimeoutMillis);
        config.setSoftMinEvictableIdleTimeMillis(softMinEvictableIdleTimeMillis);
        config.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
        config.setEvictionPolicyClassName(evictionPolicyClassName);
        config.setTestOnCreate(testOnCreate);
        config.setTestOnBorrow(testOnBorrow);
        config.setTestOnReturn(testOnReturn);
        config.setTestWhileIdle(testWhileIdle);
        config.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        config.setBlockWhenExhausted(blockWhenExhausted);
        config.setJmxEnabled(jmxEnabled);
        config.setJmxNamePrefix(jmxNamePrefix);
        config.setJmxNameBase(jmxNameBase);
    }

}
